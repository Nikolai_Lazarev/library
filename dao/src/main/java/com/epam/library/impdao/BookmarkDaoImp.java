package com.epam.library.impdao;

import com.epam.library.connection_pool.ConnectionPool;
import com.epam.library.entity.Bookmark;
import com.epam.library.idao.IBookmarkDAO;
import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class BookmarkDaoImp implements IBookmarkDAO {

	private static ConnectionPool connectionPool = ConnectionPool.getInstance();
	private static PropertiesManager propertiesManager = PropertiesManager.getInstance();
	private static Logger logger = Logger.getLogger(BookmarkDaoImp.class);

	@Override
	public List<Bookmark> findAll() {
		Connection connection = null;
		List<Bookmark> result = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from Bookmark");
			     ResultSet resultSet = statement.executeQuery()) {
				result = new ArrayList<Bookmark>();
				Bookmark bookmark = null;
				while (resultSet.next()) {
					bookmark = new Bookmark(resultSet.getInt("userId"), resultSet.getInt("bookId"),
							resultSet.getInt("page"), resultSet.getBoolean("active"));
					result.add(bookmark);
				}
			}
		} catch (SQLException e) {
			System.err.println(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}

		return result;
	}

	public List<Bookmark> findAll(Integer id) {
		Connection connection = null;
		List<Bookmark> result = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from Bookmark where userId = ?")) {
				statement.setInt(1, id);
				try(ResultSet resultSet = statement.executeQuery()) {
					result = new ArrayList<Bookmark>();
					Bookmark bookmark = null;
					while (resultSet.next()) {
						bookmark = new Bookmark(resultSet.getInt("userId"), resultSet.getInt("bookId"),
								resultSet.getInt("page"), resultSet.getBoolean("active"));
						result.add(bookmark);
					}
				}
			}
		} catch (SQLException e) {
			System.err.println(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}

		return result;
	}


	@Override
	public boolean delete(Integer id) {
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("update Bookmark set active = 0 where id = ?")) {
				statement.setInt(1, id);
				statement.executeUpdate();
				return true;
			}
		} catch (SQLException e) {
			System.err.println(e);

		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return false;
	}

	@Override
	public boolean create(Bookmark bookmark) {
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("insert into Bookmark(userId, bookId, page, active) values (?, ?, ?, true)")) {
				statement.setInt(1, bookmark.getUserId());
				statement.setInt(2, bookmark.getBookId());
				statement.setInt(3, bookmark.getPage());
				statement.executeUpdate();
				return true;
			}
		} catch (SQLException e) {
			System.err.println(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return false;
	}

	@Override
	public Bookmark findById(Integer id) {
		Connection connection = null;
		Bookmark bookmark = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from Bookmark where id = ?")) {
				statement.setInt(1, id);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						bookmark = new Bookmark(resultSet.getInt("userId"), resultSet.getInt("bookId"),
								resultSet.getInt("page"), resultSet.getBoolean("active"));
					}
				}
			}
		} catch (SQLException e) {
			System.err.println(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return bookmark;
	}
}
