package com.epam.library.idao;

import com.epam.library.entity.Entity;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;



public interface ILibraryDAO <K,T extends Entity> {
	List<T> findAll();
	boolean delete(K id);
	boolean create(T t);
	T findById(K id);

	default void close(Statement statement){
		try{
			if (statement != null){
				statement.close();
			}
		}catch (SQLException e){
			//log
		}
	}
	default void close(Connection connection){
		try{
			if(null != connection){
				connection.close();
			}
		}catch (SQLException e){
			//log
		}
	}
}
