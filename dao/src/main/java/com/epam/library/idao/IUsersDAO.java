package com.epam.library.idao;

import com.epam.library.entity.Users;

import java.util.ArrayList;

public interface IUsersDAO extends ILibraryDAO<Integer, Users>{
	ArrayList<Users> findByRole(int role);
	Users findByLoginAndPassword(String login, String password);
}
