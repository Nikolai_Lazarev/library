package com.epam.library.idao;


import com.epam.library.entity.Author;

public interface IAuthorDAO extends ILibraryDAO<Integer , Author> {
	Author getAuthorByName(String name);
}
