package com.epam.library.manager;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Pattern;

public class PropertiesManager {

	private Properties properties;
	private String db;
	private String user;
	private String password;
	private int port;
	private String ip;
	private static PropertiesManager instance = null;

	private PropertiesManager(){
		properties = new Properties();
		try {
			properties.load(getClass().getResourceAsStream("/db.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		db = properties.getProperty("dbconnection");
		user = properties.getProperty("user");
		password = properties.getProperty("password");
		port = Integer.valueOf(properties.getProperty("port"));
		ip = properties.getProperty("ipaddres");
	}

	public static PropertiesManager getInstance() {
		if(null == instance){
			return new PropertiesManager();
		}
		return instance;
	}

	public Pattern getDatePattern(){
		properties.getProperty("date_pattern");
		return Pattern.compile(properties.getProperty("date_pattern"));
	}

	public String getDb() {
		return db;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public int getPort() {
		return port;
	}

	public String getIp() {
		return ip;
	}
}
