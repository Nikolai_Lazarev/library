package com.epam.library;

public class AdminInterfaceFactory implements IUserInterfaceFactory {
	@Override
	public ISystemUserInterface createInterface(int id, TCPConnection tcpConnection) {
		return new AdministratorInterface(id, tcpConnection);
	}
}
