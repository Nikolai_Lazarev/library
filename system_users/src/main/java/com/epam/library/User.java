package com.epam.library;

import com.epam.library.manager.UserQuery;

import java.io.IOException;

public class User extends SystemUser {

	private ISystemUserInterface userInterface;

	public User(int id, String login, IUserInterfaceFactory userInterfaceFactory) throws IOException {
		super(id, login);
		userInterface = userInterfaceFactory.createInterface(id, tcpConnection);
		UserQuery query;
		String input;
		while (userInSystem) {
			userInterface.printCommandList();
			input = scanner.nextLine();
			if ("exit".equals(input.toLowerCase())) {
				exit(tcpConnection);
			} else if (null != (query = userInterface.doCommand(input))) {
				tcpConnection.sendMessage(query);
			}
		}
	}

	@Override
	public void onConnectionReady(TCPConnection connection) {

	}

	@Override
	public void onMessage(TCPConnection connection, byte[] message) {
		printMessage(message);
		System.out.println("-------------------------------");
	}


	@Override
	public void onDisconnection(TCPConnection connection) {

	}
}
