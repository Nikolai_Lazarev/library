package com.epam.library;

import com.epam.library.manager.PropertiesManager;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.util.Scanner;

public abstract class SystemUser implements ISystemUser, Serializable {

	protected static PropertiesManager propertiesManager = PropertiesManager.getInstance();
	protected Logger logger = Logger.getLogger(this.getClass());
	protected TCPConnection tcpConnection;
	protected boolean userInSystem;
	protected Scanner scanner;
	protected String login;
	protected int id;

	protected SystemUser() {
	}

	protected SystemUser(int id, String login) throws IOException {
		this.id = id;
		this.login = login;
		tcpConnection = new TCPConnection(propertiesManager.getIp(), propertiesManager.getPort(), this);
		userInSystem = true;
		scanner = new Scanner(System.in);
	}

	protected void printMessage(byte[] message) {
		if (SerializationUtils.deserialize(message) instanceof IExecuteQuery) {
			IExecuteQuery result = SerializationUtils.deserialize(message);
			System.out.println(result);
		}
	}

	protected void exit(TCPConnection connection) {
		try {
			userInSystem = false;
			connection.disconnection();
			new NotAuthorized();
		} catch (IOException e) {
			logger.error(e);
		}
	}

}
