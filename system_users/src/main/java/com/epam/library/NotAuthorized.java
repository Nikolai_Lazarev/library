package com.epam.library;

import com.epam.library.entity.Users;
import com.epam.library.manager.QueryType;
import com.epam.library.manager.UserQuery;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;
import java.util.Scanner;

public class NotAuthorized extends SystemUser {

	private static TCPConnection connection;

	public NotAuthorized() throws IOException {
		connection = new TCPConnection(propertiesManager.getIp(), propertiesManager.getPort(), this);
	}

	public void Authorization() {
		Scanner scanner = new Scanner(System.in);
		String login;
		String password;
		logger.info("Введите логин");
		login = scanner.nextLine();
		logger.info("Введите пароль");
		password = scanner.nextLine();
		connection.sendMessage(new UserQuery(QueryType.AUTHORIZATION, login, password));
	}


	@Override
	public void onConnectionReady(TCPConnection connection) {
		Authorization();
	}

	@Override
	public void onMessage(TCPConnection connection, byte[] message) {
		if (SerializationUtils.deserialize(message) instanceof Users) {
			Users user = SerializationUtils.deserialize(message);
			SystemUser systemUser = null;
			try {
				switch (user.getRole()) {
					case 1:
						connection.disconnection();
						systemUser = new Administrator(user.getId(), user.getLogin(), new AdminInterfaceFactory());
						break;
					case 2:
						connection.disconnection();
						systemUser = new User(user.getId(), user.getLogin(), new UserInterfaceFactory());
						break;
					default: Authorization();
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}if(SerializationUtils.deserialize(message) instanceof ExecuteQueryError){
			ExecuteQueryError err = SerializationUtils.deserialize(message);
			System.out.println(err);
			Authorization();
		}
	}


	@Override
	public void onDisconnection(TCPConnection connection) {

	}
}
