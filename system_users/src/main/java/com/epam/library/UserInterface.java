package com.epam.library;

import com.epam.library.manager.PropertiesManager;
import com.epam.library.manager.QueryType;
import com.epam.library.manager.UserQuery;
import org.apache.log4j.Logger;

import java.util.Scanner;
import java.util.regex.Matcher;

public class UserInterface implements ISystemUserInterface {

	private static Logger logger = Logger.getLogger(UserInterface.class);
	protected static PropertiesManager propertiesManager = PropertiesManager.getInstance();
	protected int id;
	protected TCPConnection tcpConnection;

	UserInterface(int id, TCPConnection tcpConnection) {
		this.id = id;
		this.tcpConnection = tcpConnection;
	}

	@Override
	public void printCommandList() {
		logger.info("\nexit - выйти\n1: Добавить закладку\n" +
				"2: Все закладки\n" +
				"3: Поиск книги");
	}

	@Override
	public UserQuery doCommand(String command) {
		switch (command.trim()) {
			case "1":
				return addBookmark();
			case "2":
				return showAllBookmark();
			case "3":
				return searchBooks();
			default:
				return null;

		}
	}

	protected UserQuery showAllBooks() {
		String args[] = new String[0];
		Scanner scanner = new Scanner(System.in);
		System.out.println("Нажмите enter для продолжения или введите -1 для выхода");
		if ("-1".equals(scanner.nextLine())) return null;
		return new UserQuery(QueryType.SHOW_BOOKS);
	}

	protected UserQuery addBookmark() {
		tcpConnection.sendMessage(new UserQuery(QueryType.SHOW_BOOKS, new String[]{}));
		String args[] = new String[3];
		Scanner scanner = new Scanner(System.in);
		args[0] = Integer.toString(id);
		System.out.println("Введите ISBN книги или -1 для выхода");
		args[1] = scanner.nextLine();
		if ("-1".equals(args[1])) return null;
		System.out.println("Номер страницы");
		args[2] = scanner.nextLine();
		return new UserQuery(QueryType.ADD_BOOKMARK, args);
	}

	protected UserQuery searchBooks() {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			logger.info("\n1: Поиск по ISBN\n" +
					"2: Поиск по названию\n" +
					"3: Поиск по автору\n" +
					"4: Поиск по промежутку дат\n" +
					"5: Поиск по всем критериям\n" +
					"6: Все книги\n" +
					"7: Назад"
			);
			switch (scanner.nextInt()) {
				case 1:
					return searchBookByISBN();
				case 2:
					return searchBookByName();
				case 3:
					return searchBookByAuthorName();
				case 4:
					return searchBookByDate();
				case 5:
					return searchBookByAllCriterion();
				case 6:
					return showAllBooks();
				case 7:
					return null;
				default:
					logger.trace("Введите корректную команду");
			}
		}
	}

	protected UserQuery searchBookByDate(){
		String args[] = new String[2];
		Scanner scanner = new Scanner(System.in);
		logger.info("Введите дату выпуска в формате XXXX-XX-XX");
		logger.info("От");
		args[0] = scanner.nextLine();
		Matcher matcher = propertiesManager.getDatePattern().matcher(args[0]);
		while(!matcher.matches()){
			args[0] = scanner.nextLine();
			matcher = propertiesManager.getDatePattern().matcher(args[0]);
		}
		logger.info("До");
		args[1] = scanner.nextLine();
		while(!matcher.matches()){
			args[1] = scanner.nextLine();
			matcher = propertiesManager.getDatePattern().matcher(args[1]);
		}
		for(String x : args){
			if(!x.equals("")){
				//return new UserQuery();
			}
		}
		return new UserQuery(QueryType.SEARCH_BOOK_BY_DATE, args);
	}

	protected UserQuery searchBookByName() {
		Scanner scanner = new Scanner(System.in);
		String args[] = new String[1];
		String input;
		logger.info("Введите название книги или -1 для выхода");
		while ((input = scanner.nextLine()).length() > 40) {
			if ("-1".equals(input)) return null;
			logger.info("Название книги должно быть не длинее 40 символов");
		}
		args[0] = input;
		return new UserQuery(QueryType.SEARCH_BOOK_BY_NAME, args);
	}

	protected UserQuery searchBookByAllCriterion() {
		String args[] = new String[4];
		Scanner scanner = new Scanner(System.in);
		logger.info("Введите название книги или -1 для выхода");
		args[0] = scanner.nextLine();
		if ("-1".equals(args[0])) return null;
		logger.info("Введите автора");
		args[1] = scanner.nextLine();
		logger.info("Введите дату выпуска в формате XXXX-XX-XX");
		logger.info("От");
		args[2] = scanner.nextLine();
		Matcher matcher = propertiesManager.getDatePattern().matcher(args[2]);
		while(!matcher.matches()){
			args[2] = scanner.nextLine();
			matcher = propertiesManager.getDatePattern().matcher(args[2]);
		}
		logger.info("До");
		args[3] = scanner.nextLine();
		while(!matcher.matches()){
			args[3] = scanner.nextLine();
			matcher = propertiesManager.getDatePattern().matcher(args[3]);
		}
		for(String x: args){
			if(!x.trim().equals("")){
				return new UserQuery(QueryType.SEARCH_BOOK_BY_ALL_CRITERION, args);
			}
		}
		return new UserQuery(QueryType.SHOW_BOOKS, args);
	}

	protected UserQuery searchBookByAuthorName() {
		Scanner scanner = new Scanner(System.in);
		String args[] = new String[1];
		String input;
		logger.info("Введите имя автора или - 1 для выхода");
		while ((input = scanner.nextLine()).length() > 30) {
			if ("-1".equals(input)) return null;
			logger.info("Длина имени не может превышать 30 символов");
		}
		args[0] = input;
		return new UserQuery(QueryType.SEARCH_BOOK_BY_AUTHOR, args);
	}

	protected UserQuery searchBookByISBN() {
		Scanner scanner = new Scanner(System.in);
		String args[] = new String[1];
		logger.info("Введите ISBN или -1 для выхода");
		args[0] = scanner.nextLine();
		if ("-1".equals(args[0])) return null;
		return new UserQuery(QueryType.SEARCH_BOOK_BY_ISBN, args);
	}

	protected UserQuery showAllBookmark() {
		System.out.println("Нажмите enter для продолжения или введите -1 для выхода");
		Scanner scanner = new Scanner(System.in);
		if ("-1".equals(scanner.nextLine())) return null;
		String args[] = new String[1];
		args[0] = String.valueOf(id);
		return new UserQuery(QueryType.SHOW_BOOKMARK, args);
	}

}
