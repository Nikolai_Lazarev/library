package com.epam.library;

public class ExecuteQuerySuccess implements IExecuteQuery {

	private String message;

	public ExecuteQuerySuccess() {
		message = "Запрос выполнен успешно.";
	}

	public ExecuteQuerySuccess(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "QuerySuccess: " + message;
	}
}
