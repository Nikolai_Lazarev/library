package com.epam.library;

public class ExecuteQueryError implements IExecuteQuery {

	private String message;

	ExecuteQueryError() {
		message = "Не удалось выполнить запрос";
	}

	ExecuteQueryError(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "QueryError: " + message;
	}
}
