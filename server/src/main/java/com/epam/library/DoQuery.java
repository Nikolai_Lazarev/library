package com.epam.library;

import com.epam.library.entity.Author;
import com.epam.library.entity.Bookmark;
import com.epam.library.entity.Books;
import com.epam.library.entity.Users;
import com.epam.library.impdao.AuthorDaoImp;
import com.epam.library.impdao.BookDaoImp;
import com.epam.library.impdao.BookmarkDaoImp;
import com.epam.library.impdao.UsersDaoImp;
import com.epam.library.manager.QueryType;
import com.epam.library.manager.UserQuery;

import java.util.ArrayList;
import java.util.List;

public class DoQuery {


	public static List<IExecuteQuery> doThisQuery(UserQuery userQuery) {
		String[] args = userQuery.getArgs();
		QueryType type = userQuery.getType();
		BookDaoImp bookDaoImp = new BookDaoImp();
		AuthorDaoImp authorDaoImp = new AuthorDaoImp();
		UsersDaoImp usersDaoImp = new UsersDaoImp();
		BookmarkDaoImp bookmarkDaoImp = new BookmarkDaoImp();
		ArrayList<IExecuteQuery> result;
		Books book = null;
		switch (type) {
			case ADD_BOOK:
				Author checkAuthor = authorDaoImp.getAuthorByName(args[1]);
				result = new ArrayList<IExecuteQuery>();
				if (args[3].equals("0")) {
					if (checkAuthor != null) {
						if (bookDaoImp.create(new Books(args[0], checkAuthor.getId(), args[2]))) {
							result.add(new ExecuteQuerySuccess("Книга добавлена"));
						} else {
							result.add(new ExecuteQueryError("Не удалось добавить книгу"));
						}
					} else {
						result.add(new ExecuteQueryError("Не удалось добавить книгу"));
					}
				} else if (args[3].equals("1")) {
					if (checkAuthor != null) {
						if (bookDaoImp.create(new Books(args[0], checkAuthor.getId(), args[2]))) {
							result.add(new ExecuteQuerySuccess("Книга добавлена"));
						} else {
							result.add(new ExecuteQueryError("Не удалось добавить книгу"));
						}
					} else {
						if (authorDaoImp.create(new Author(args[1]))) {
							checkAuthor = authorDaoImp.getAuthorByName(args[1]);
							if (bookDaoImp.create(new Books(args[0], checkAuthor.getId(), args[2]))) {
								result.add(new ExecuteQuerySuccess("Книга добавлена"));
							} else {
								result.add(new ExecuteQueryError("Не удалось добавить книгу"));
							}
						}
					}
				}
				return result;
			case DELETE_BOOK:
				result = new ArrayList<IExecuteQuery>();
				if (bookDaoImp.delete(Integer.valueOf(args[0]))) {
					result.add(new ExecuteQuerySuccess("Книги удалена"));
				} else {
					result.add(new ExecuteQueryError("Не удалось удалить книгу"));
				}
				return result;
			case SET_ROLE:
				if (usersDaoImp.setRoleById(Integer.valueOf(args[0]), Integer.valueOf(args[1]))) {
					result = new ArrayList<IExecuteQuery>();
					result.add(new ExecuteQuerySuccess("Роль назначена"));
				} else {
					result = new ArrayList<IExecuteQuery>();
					result.add(new ExecuteQueryError("Не удалоись назначить роль"));
				}
				return result;
			case ADD_USER:
				result = new ArrayList<IExecuteQuery>();
				if (usersDaoImp.create(new Users(args[0], args[1], Integer.valueOf(args[2])))) {
					result.add(new ExecuteQuerySuccess("Пользователь добавлен"));
				} else {
					result.add(new ExecuteQueryError("Не удалось добавить пользователя"));
				}
				return result;
			case AUTHORIZATION:
				result = new ArrayList<IExecuteQuery>();
				Users user = usersDaoImp.findByLoginAndPassword(args[0], args[1]);
				if (null == user) {
					result.add(new ExecuteQueryError("Не удалось найти пользователя"));
				} else {
					result.add(user);
				}
				return result;
			case ADD_BOOKMARK:
				result = new ArrayList<IExecuteQuery>();
				if (bookmarkDaoImp.create(new Bookmark(Integer.valueOf(args[0]), Integer.valueOf(args[1]), Integer.valueOf(args[2])))) {
					result.add(new ExecuteQuerySuccess("Закладка добавлена"));
				} else {
					result.add(new ExecuteQueryError("Не удалось добавить закладку"));
				}
				return result;
			case DELETE_BOOKMARK:
				result = new ArrayList<IExecuteQuery>();
				if (bookmarkDaoImp.delete(Integer.valueOf(args[0]))) {
					result.add(new ExecuteQuerySuccess("Закладка удалена"));
				} else {
					result.add(new ExecuteQueryError("Не удалось удалить закладку"));
				}
				return result;
			case SEARCH_BOOK_BY_ISBN:
				result = new ArrayList<IExecuteQuery>();
				result.add(bookDaoImp.findById(Integer.valueOf(args[0])));
				if (result.isEmpty()) {
					result.add(new ExecuteQueryError("Записей нет"));
				}
				return result;
			case SHOW_BOOKMARK:
				result = new ArrayList<IExecuteQuery>();
				result.addAll(bookmarkDaoImp.findAll(Integer.valueOf(args[0])));
				if (result.isEmpty()) {
					result.add(new ExecuteQueryError("Закладок нет"));
				}
				return result;
			case SHOW_BOOKS:
				result = new ArrayList<IExecuteQuery>();
				result.addAll(bookDaoImp.findAll());
				if (result.isEmpty()) {
					result.add(new ExecuteQueryError("Книг нет"));
				}
				return result;
			case SEARCH_BOOK_BY_NAME:
				result = new ArrayList<IExecuteQuery>();
				result.add(bookDaoImp.getBookByName(args[0]));
				if (result.isEmpty()) {
					result.add(new ExecuteQueryError("Книг нет"));
				}
				return result;
			case SEARCH_BOOK_BY_AUTHOR:
				result = new ArrayList<IExecuteQuery>();
				result.addAll(bookDaoImp.getBookByAuthor(args[0]));
				if (result.isEmpty()) {
					result.add(new ExecuteQueryError("Книг нет"));
				}
				return result;
			case SEARCH_BOOK_BY_ALL_CRITERION:
				result = new ArrayList<>();
				result.addAll(bookDaoImp.getBookByAllCriterion(args));
				if (result.isEmpty()) {
					result.add(new ExecuteQueryError("Книг нет"));
				}
				return result;
			case SEARCH_BOOK_BY_DATE:
				result = new ArrayList<>();
				result.addAll(bookDaoImp.searchBookByDate(args));
				if (result.isEmpty()) {
					result.add(new ExecuteQueryError("Книг не найдено"));
				}
				return result;
			case GET_USERS_LIST:
				result = new ArrayList<>();
				result.addAll(usersDaoImp.findAll());
				return result;
			default:
		}
		result = new ArrayList<IExecuteQuery>();
		result.add(new ExecuteQueryError());
		return result;
	}

}
