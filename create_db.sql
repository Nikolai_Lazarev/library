create database library;
use library;

create table Roles(
id int primary key auto_increment NOT NULL,
name varchar(10) NOT NULL
);

create table Users(
id int primary key auto_increment NOT NULL,
login varchar(30) NOT NULL,
password varchar(30) NOT NULL,
role int NOT NULL,
foreign key (role) references Roles(id)
);

create table Authors(
id int primary key auto_increment NOT NULL,
name varchar(40) NOT NULL
);

create table Books(
ISBN int primary key auto_increment NOT NULL,
name varchar(40) NOT NULL,
authorId int NOT NULL,
issueYear date,
foreign key (authorId) references Authors(id) 
);

create table Bookmark(
userId int NOT NULL,
bookId int NOT NULL,
page int NOT NULL,
foreign key (bookId) references Books(ISBN),
foreign key (userId) references Users(id) 
);

insert into Roles(name) value ('admin');
insert into Roles(name) value ('user');
insert into Users(login, password, role) value ('admin', '1234', 1);
