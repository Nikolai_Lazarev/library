package com.epam.library.entity;


public class Books extends Entity {
	private int ISBN;
	private String name;
	private int authorId;
	private String authorName;
	private String issueYear = "Неизвестно";

	public Books(int ISBN, String name, int authorId, String issueYear) {
		this.ISBN = ISBN;
		this.name = name;
		this.authorId = authorId;
		this.issueYear = issueYear;
	}

	public Books(String name, int authorId, String issueYear) {
		this.name = name;
		this.authorId = authorId;
		this.issueYear = issueYear;
	}

	public Books(int ISBN, String name, int authorId) {
		this.ISBN = ISBN;
		this.name = name;
		this.authorId = authorId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAuthor(int author) {
		this.authorId = author;
	}

	public void setIssueYear(String issueYear) {
		this.issueYear = issueYear;
	}

	public void setISBN(int ISBN) {
		this.ISBN = ISBN;
	}

	public int getISBN() {
		return ISBN;
	}

	public String getName() {
		return name;
	}

	public int getAuthor() {
		return authorId;
	}

	public String getIssueYear() {
		return issueYear;
	}

	@Override
	public String toString() {
		return "ISBN: " + ISBN +
				"\nназвание: '" + name + '\'' +
				"\nID автора: " + authorId +
				"\nдата выпуска: " + issueYear;
	}
}
