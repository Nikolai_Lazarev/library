package com.epam.library.entity;

public class Author extends Entity {
	private int id;
	private String name;

	public Author(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public Author(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Имя: " + name +
				"\nId: " + id;
	}
}
